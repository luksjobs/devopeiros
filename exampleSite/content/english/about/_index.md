---
title: "Hello there, eu sou Lucas Oliveira 👋"
meta_title: "About"
description: "Descrição sobre o projeto Devopeiros"
image: "/images/avatar.png"
draft: false
---

## Projeto "Devopeiros"

Inicialmente, vamos falar sobre o projeto `Devopeiros` que é um projeto dedicado a fornecer informações e recursos relacionados ao mundo do desenvolvimento de software e tecnologia. Nossa missão é compartilhar conhecimento, dicas e truques para ajudar a comunidade devops.

## O Curso

O Curso: "**Aprenda tudo sobre Docker do básico ao avançado e dê start na sua jornada Devops**!" é um curso ‘Hands-on’, mão na massa sem enrolações, montagem de laboratórios e testes feitos na raça! Ao concluir este curso, você estará equipado com as habilidades necessárias para criar, implantar e gerenciar aplicativos em contêineres Docker, desde o nível básico até os tópicos avançados de orquestração e implantação.

## Conhecendo o Instrutor

Se você veio até aqui é porque você sente a necessidade de estar sempre aprendendo mais, se qualificando e se preparando para este mercado de trabalho  que o Devops proporciona... função que está bem aquecida... 

### Então, conheça um pouco sobre mim:

Desde o ínicio da minha carreira com Devops, a computação em Nuvem e o fato de trabalhar com **Docker**, **Kubernetes**, **Pipelines** e todas as outras ferramentas que o universo Devops proporciona tem sido minha paixão! Ainda me lembro de sentar na frente do meu computador, criando uma máquina virtual, hospedando uma página web de "Olá Mundo!!" usando um servidor web Apache. Desde então, tenho sido obcecado com a ideia de fornecer conteúdo para o mundo remotamente.

Essa paixão me levou a explorar o conceito de *virtualização*. Instalar o Xen (Hypervisor Tipo 1) em algumas máquinas antigas e hospedar um aplicativo baseado em armazenamento marcou meu primeiro passo em direção à nuvem. Aprendendo sobre vários provedores de nuvem como **AWS**, **AZURE**, **GCP**, percebi o quanto mais havia para aprender. E assim, aprendi Tecnologias em Nuvem com praticamente a mesma fascinação que me impulsionava quando eu era estudante.

Agora, na indústria, essa paixão ainda permanece comigo. Certamente há mais a aprender e ainda mais para migrar para a nuvem.