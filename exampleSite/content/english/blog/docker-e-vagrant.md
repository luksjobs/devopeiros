---
author: "Lucas Oliveira"
title: "Rodando o Docker dentro de uma máquina Vagrant"
date: "2023-08-17"
image: "/images/image-placeholder.png"
categories: ["Vagrant", "Docker"]
description: "As pipelines do Azure DevOps são uma ferramenta poderosa para automatizar e gerenciar o fluxo de trabalho de desenvolvimento de software."
tags: ["Vagrant", "Vagrantfile", "Docker"]
---

Este guia tem como objetivo fornecer uma explicação sobre o conceito do **Vagrant** e demonstrar como é possível executar o `Docker` dentro de uma máquina virtual criada com o Vagrant. O Vagrant é uma ferramenta de código aberto que auxilia na criação e gerenciamento de ambientes de desenvolvimento virtualizados. Ele permite criar máquinas virtuais consistentes e portáteis, tornando mais fácil a configuração e compartilhamento de ambientes entre membros de uma equipe.

<!--more-->

## Conceito do Vagrant

O Vagrant simplifica a criação e gerenciamento de ambientes de desenvolvimento, permitindo que você defina as configurações de uma máquina virtual em um único arquivo, chamado de "**Vagrantfile**". Esse arquivo descreve os detalhes da máquina virtual, como sistema operacional, recursos de hardware, configurações de rede e provisionamento.

### Principais vantagens do Vagrant:

* **Reprodutibilidade**: Com um Vagrantfile, você pode criar a mesma máquina virtual em qualquer lugar, garantindo que todos os membros da equipe tenham o mesmo ambiente de desenvolvimento.
* **Portabilidade**: Mova facilmente seus ambientes entre diferentes sistemas operacionais ou máquinas físicas.
* **Provisionamento automatizado**: Configure automaticamente sua máquina virtual com scripts de provisionamento, garantindo que todas as dependências necessárias sejam instaladas corretamente.

{{< notice "Dica:" >}}
Sim, é possível rodar o **Docker** dentro de uma máquina virtual criada pelo **Vagrant**. Isso pode ser útil quando você deseja isolar seus ambientes de desenvolvimento ou testar configurações específicas antes de implantá-las em um ambiente de produção.
{{< /notice >}}

## Emulando o Docker no Vagrant:

Crie um arquivo chmado de **`Vagrantfile`**: Adicione as configurações da máquina virtual e o provisionamento necessário para instalar o Docker.

```
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/jammy64" # Escolha a box que você deseja usar
  config.vm.define "docker-node-01" do |docker|
  config.vm.box_download_insecure=true #Bypass nas imagens não consideradas "oficiais"  
  config.vm.provision "shell", inline: <<-SHELL
    # Atualiza a lista de pacotes
    sudo apt-get update

    # Instala pacotes essenciais
    sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common

    # Adiciona a chave GPG oficial do Docker
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

    # Adiciona o repositório do Docker
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

    # Atualiza a lista de pacotes novamente
    sudo apt-get update

    # Instala o Docker
    sudo apt-get install -y docker-ce

    # Adiciona o usuário ao grupo do Docker para não precisar usar sudo
    sudo usermod -aG docker vagrant
  SHELL
  end
end
```

Inicie a máquina virtual:

```
$ vagrant up
```

Conecte-se à máquina virtual via SSH:

```
$ vagrant ssh
```

Dentro da máquina virtual, verifique se o Docker está funcionando:

```
$ docker --version
```
Agora você tem uma máquina virtual configurada pelo Vagrant com o Docker instalado. Você pode executar e gerenciar contêineres Docker dentro dessa máquina virtual como faria em qualquer outro ambiente Docker.
Lembre-se de que este é apenas um exemplo básico. Você pode personalizar o Vagrantfile e o provisionamento de acordo com suas necessidades específicas.

## Explicando o **Vagrantfile**

* `Vagrant.configure("2") do |config|`: Inicia a configuração do Vagrant usando a versão 2 da API.

* `config.vm.box = "ubuntu/jammy64"`: Define a box a ser usada para criar a máquina virtual. Nesse caso, está usando a box "ubuntu/jammy64", que provavelmente é uma imagem Ubuntu para a versão "Jammy" do sistema.

* `config.vm.define "docker-node-01" do |docker|`: Define uma máquina virtual com o nome "docker-node-01" usando a configuração interna "docker".

* `config.vm.box_download_insecure=true`: Habilita o download de boxes não oficiais, permitindo que você use uma box que não seja das fontes oficiais do Vagrant.

* `config.vm.provision "shell", inline: <<-SHELL ... end`: Especifica uma provisão do tipo "shell" que será executada na máquina virtual após ela ser criada. O bloco inline: contém um script shell que será executado no ambiente da máquina virtual. Esse script realiza as seguintes ações:

* `end`: Encerra o bloco de configuração da máquina virtual "docker-node-01".
* `end`: Encerra o bloco de configuração do Vagrantfile.

Esse **Vagrantfile** cria uma máquina virtual chamada *"docker-node-01"* baseada na box *"ubuntu/jammy64"*, instala o Docker e configura o usuário "vagrant" para executar comandos Docker sem a necessidade de usar sudo. Lembre-se de que esse é apenas um exemplo e pode ser ajustado de acordo com suas necessidades e preferências. Certifique-se de entender cada etapa antes de usar esse Vagrantfile em um ambiente de produção.