---
author: "Lucas Oliveira"
title: "Tudo sobre Git - (Guia Prático)"
date: "2023-09-20"
image: "/images/git.png"
categories: ["Git"]
description: "Guia de como utilizar o git de forma profissional seguindo as melhores práticas"
tags: ["git", "gitflow", "devops"]
---

O **Git** é uma das ferramentas mais essenciais para desenvolvedores e equipes de desenvolvimento de software em todo o mundo. É uma ferramenta de controle de versão distribuída que oferece inúmeros benefícios para o gerenciamento eficiente de código-fonte. 

<!--more-->

> "Em um mundo DevOps, o Git é o compasso que guia os desenvolvedores, permitindo-lhes navegar com confiança pelos mares da colaboração e do versionamento de código." 

Neste post, vamos explorar os principais benefícíos de usar o Git e listar alguns dos comandos mais utilizados. Começaremos pela lista dos beneficíos logo abaixo: 

- **Benefício Principal**: Controle de Versão Distribuído — o principal benefício de usar o Git é o controle de versão distribuído. Isso significa que, em vez de depender de um repositório centralizado, cada desenvolvedor possui uma cópia completa do repositório em sua máquina local. Isso traz várias vantagens:

- **Independência**: Cada desenvolvedor pode trabalhar em seu próprio ramo (branch) sem interferir no trabalho dos outros. - Isso permite o desenvolvimento paralelo de recursos, correções de bugs e experimentação sem atritos.

- **Histórico Completo**: O Git mantém um histórico completo de todas as alterações feitas no código-fonte. Isso facilita a identificação de quando e por quem uma alteração foi feita, o que é essencial para a depuração e auditoria.

- **Segurança**: Como cada desenvolvedor possui uma cópia completa do repositório, os dados estão descentralizados e seguros contra perda. Se um servidor central falhar, as cópias locais dos desenvolvedores ainda estão disponíveis.

- **Facilidade de Colaboração**: Os desenvolvedores podem colaborar facilmente em projetos, mesclando (merging) suas alterações quando estiverem prontas. Isso reduz conflitos e facilita o trabalho em equipe.

## Workflows

{{< image src="images/git-flow.png" caption="" alt="alter-text" height="" width="" position="center" command="fill" option="q100" class="img-fluid" title="git-flow"  webp="false" >}}

São, literalmente, o método utilizado para interagir com o código. Em outras palavras, `os workflows vão refletir a forma como o código é gerenciado e como seu ciclo de vida funciona``. Dois conceitos fundamentais de workflow são o *Branching* e o *Forking*. Vamos falar um pouco destas duas funcionalidades sem entrar muito em detalhe na parte processual de workflows. Caso tenha interesse em alguns modelos, este link se refere aos mais comuns.

## Branching

{{< image src="images/branching.png" caption="" alt="alter-text" height="" width="" position="center" command="fill" option="q100" class="img-fluid" title="git-flow"  webp="false" >}}

Em um determinado repositório, a linha principal de código é conhecida como “`master branch`”, ou somente “`master`”. Uma das maneiras de divergir desta linha principal de código é criando um “branch”. Este branch encapsula todas as alterações em uma linha independente do master, que pode ser testada e revisada de forma totalmente isolada (mas ainda sim, sob o mesmo repositório). Após os devidos testes e revisões, este branch pode ser incorporado (ou merge) ao master.

## Forking

O **Forking** permite divergir do master a partir da criação de uma cópia independente do repositório a ser clonado. Essa prática é mais utilizada quando o organizações distintas trabalham no mesmo repositório — como em projetos open source. Desta forma, cada alteração é feita em um repositório independente, e possivelmente submetido “upstream” para aprovação e merge. A partir daí, basta cada desenvolvedor sincronizar fork com o repositório principal para atualizar o estado do fork.


Vários usuários interagindo com seus respectivos forks, e submetendo alterações upstream ainda sob o mesmo fork, podemos criar vários branches para trabalhar simultaneamente. Ou seja, trabalhar em um fork não te impede de usar branches para gerenciar melhor as alterações.

## Comandos Git Mais Utilizados

Aqui estão alguns dos comandos Git mais utilizados, juntamente com suas descrições:

```
$ git init | Inicializa um novo repositório Git em um diretório local.

$ git clone <URL> | Clona um repositório remoto para sua máquina local.

$ git add <arquivo> | Adiciona um arquivo ou alteração ao índice (staging area) para prepará-lo para commit.

$ git commit -m "mensagem" | Registra as alterações no repositório com um a mensagem descritiva.

$ git pull: Obtém as últimas alterações do repositório remoto e mescla-as com seu ramo local.

$ git push |  Envia suas alterações locais para o repositório remoto.

$ git branch |  Lista os ramos disponíveis e indica o ramo atual com um asterisco.

$ git checkout <ramo> |  Muda para outro ramo no repositório.

$ git merge <ramo> |  Mescla as alterações de outro ramo no ramo atual.

$ git status |  Exibe o estado atual do seu diretório de trabalho, mostrando os arquivos modificados.

$ git log |  Mostra um histórico de commits no ramo atual.

$ git stash |  Salva temporariamente as alterações não comprometidas, permitindo que você as recupere mais tarde.

$ git remote -v |  Exibe as URLs dos repositórios remotos associados ao seu repositório local.

$ git reset <arquivo> |  Remove um arquivo do índice, mas mantém as alterações no diretório de trabalho.

$ git diff |  Mostra as diferenças entre os arquivos no índice (staging area) e os arquivos no diretório de trabalho.
```

Lembre-se de que este é apenas um conjunto inicial de comandos Git. O Git oferece uma ampla variedade de recursos e comandos para atender a várias necessidades de controle de versão. À medida que você se familiariza com o Git, você pode explorar comandos adicionais para aprimorar seu fluxo de trabalho de desenvolvimento.

## Conclusão

O Git é uma ferramenta poderosa que oferece controle de versão distribuído, tornando o desenvolvimento de software mais eficiente, seguro e colaborativo. Os comandos Git mencionados acima são apenas o ponto de partida para aproveitar ao máximo o Git em seus projetos de desenvolvimento. À medida que você se aprofunda em sua jornada Git, você descobrirá muitos outros comandos e recursos que o ajudarão a gerenciar melhor seu código-fonte.