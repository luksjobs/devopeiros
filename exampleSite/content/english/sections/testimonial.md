---
enable: true
title: "Opinião dos nossos Alunos"
description: "Depoimentos que mencionam como os alunos aplicaram os conhecimentos adquiridos do nosso curso em seus projetos e no ambiente de trabalho, confira:"

# Testimonials
testimonials:
  - name: "Kayck Mizael"
    designation: "Analista de Infraestrutura de TI"
    avatar: "/images/avatar-sm.png"
    content: "Ter concluído o curso **Docker do básico ao avançado**, foi uma experiência verdadeiramente transformadora para mim. Quando me inscrevi, eu estava buscando uma compreensão mais profunda de como integrar o Docker de maneira eficiente no meu trabalho. O que eu recebi foi muito além das minhas expectativas."

  - name: "Sara Maria"
    designation: "Assistente de Segurança da Informação"
    avatar: "/images/avatar-sm.png"
    content: "O conteúdo do curso abordou de forma abrangente todos os aspectos da cultura DevOps, desde a colaboração entre equipes até a automação de processos cruciais. As aulas eram envolventes e bem estruturadas, facilitando a compreensão de conceitos complexos. Além disso, a combinação de teoria e exercícios práticos permitiu que eu aplicasse imediatamente o que estava aprendendo."

  - name: "Camunai Rodrigues"
    designation: "Administrador de Redes"
    avatar: "/images/avatar-sm.png"
    content: "Eu recomendo este curso a qualquer pessoa que deseje aprofundar seus conhecimentos em DevOps e transformar a maneira como trabalha com desenvolvimento e operações."

  - name: "Danilo Macêdo"
    designation: "Assistente de Infraestrutura de TI"
    avatar: "/images/avatar-sm.png"
    content: "O que aprendi aqui não apenas enriqueceu meu conjunto de habilidades, mas também abriu portas para novas oportunidades de crescimento profissional."

# don't create a separate page
_build:
  render: "never"
---
