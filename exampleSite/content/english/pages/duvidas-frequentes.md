---
title: "Informações Complementares"
# meta title
meta_title: ""
# meta description
description: "Nesta página, você encontrará mais informações sobre o Curso"
# save as draft
draft: false
---

<hr>

### Dúvidas frequentes

Abaixo estão alguns tópicos de dúvidas mais frequentes, confira:

{{< tabs >}}
{{< tab "Afinal, por que estudar Devops?" >}}

 - Um "**Devopeiro**" é um profissional de tecnologia que trabalha na interface entre as áreas de **desenvolvimento** (dev) e **operações** (ops), visando integrar e automatizar processos e fluxos de trabalho para garantir maior eficiência, rapidez e qualidade na entrega de software e serviços.

 - Em resumo, estudar DevOps pode ajudá-lo a aumentar a eficiência e a agilidade no desenvolvimento e entrega de software, melhorar a colaboração entre equipes, reduzir o tempo de lançamento, melhorar a qualidade do software e aprender habilidades valiosas para o mercado de trabalho.

{{< /tab >}}

{{< tab "Qual a carga horária do Curso?" >}}

- O curso tem uma carga horária de 12 horas ao total.
- As aulas ONLINE (ao vivo) serão sempre ministradas remotamente com uma duração de 1h e 30 minutos e serão disponibilizadas no Hotmart.
- Sempre que for necessário realizar algum reajuste na data combinada, será disparado um e-mail e avisado nas plataformas de comunicação com 2h de antecedência.

{{< /tab >}}

{{< tab "Conteúdo de acesso" >}}

- Sim! Todas as aulas serão gravadas e disponibilizadas no Hotmart ao término de cada aula online (ao vivo).
- O curso contará com atividades práticas e materiais exclusivos que serão disponibilizados durante o percuso.
- Ao término do curso, você continuará fazendo parte da nossa comunidade como membro, tendo acesso a novos conteúdos.

{{< /tab >}}
{{< /tabs >}}

<hr>

### Pra quem é destinado esse treinamento?"

 - O curso é adequado tanto para iniciantes que desejam aprender sobre os conceitos mais aquecidos da área de Devops quanto para profissionais de TI que desejam aprimorar mais ainda suas habilidades nessa área.
- No curso você encontrará o Roadmap completo para quem quer iniciar ou quer avançar em suas habilidades no mundo do Devops.
- Não é necessário que você seja um expert em linux para acompanhar esse curso, pode ficar tranquilo!

### Certificado de conclusão

- A badge e o certificado internacional, possibilitam ao estudante o compartilhamento em suas redes sociais, além de disponibilizar uma página pessoal para que outras pessoas possam validar o seu certificado.