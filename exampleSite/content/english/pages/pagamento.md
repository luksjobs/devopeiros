---
title: "Pagamentos"
# meta title
meta_title: ""
# meta description
description: "Nesta página, você encontrará mais informações sobre as formas de pagamento do curso."
# save as draft
draft: false
---

### Formas de pagamento

Com o curso '**Aprenda tudo sobre Docker do básico ao avançado**', você terá a oportunidade de dominar uma das tecnologias mais essenciais para qualquer profissional de DevOps. Desde os conceitos fundamentais até técnicas avançadas, este curso abrangente oferece o conhecimento e as habilidades necessárias para gerenciar contêineres **Docker** de forma eficaz. Prepare-se para dar um grande impulso à sua carreira em DevOps, pois este curso é o seu primeiro passo para uma jornada de sucesso na automação de desenvolvimento e implantação de software.

| Curso         |      Conteúdo     |  Valor                                       |
| ------------- | :-----------:     | ----:                                        |
| Aprenda tudo sobre Docker do básico ao avançado  | Conteúdo Digital (Curso) | R$: 399,90  |

{{< image src="images/capa-do-curso.png" caption="" alt="alter-text" height="" width="" position="center" command="fill" option="q100" class="img-fluid" title="git-flow"  webp="false" >}}

<center><script type="text/javascript">
	function importHotmart(){ 
 		var imported = document.createElement('script'); 
 		imported.src = 'https://static.hotmart.com/checkout/widget.min.js'; 
 		document.head.appendChild(imported); 
		var link = document.createElement('link'); 
		link.rel = 'stylesheet'; 
		link.type = 'text/css'; 
		link.href = 'https://static.hotmart.com/css/hotmart-fb.min.css'; 
		document.head.appendChild(link);	} 
 	importHotmart(); 
 </script> 
 <a onclick="return false;" href="https://pay.hotmart.com/G87318032X?checkoutMode=2" class="hotmart-fb hotmart__button-checkout">Comprar Agora</a></center> 

### Confirmação de Pagamento

Após a conclusão bem-sucedida do pagamento para o curso '**Aprenda tudo sobre Docker do básico ao avançado**', todo o conteúdo do curso será enviado diretamente para o seu e-mail. Fique atento à sua caixa de entrada para receber os materiais e informações de acesso. As datas de início do curso também serão fornecidas junto com o conteúdo, para que você possa começar a sua jornada no mundo do Docker e do DevOps no momento mais conveniente para você."

### Informações sobre o Pagamento e Acesso ao Curso

O pagamento para o curso "*Aprenda tudo sobre Docker do básico ao avançado*" será processado pela plataforma Hotmart. 

Após a conclusão bem-sucedida do pagamento, todos os recursos, materiais do curso e informações de acesso serão disponibilizados na plataforma de acesso do Hotmart. 

Prepare-se para iniciar sua jornada de aprendizado em Docker de maneira conveniente e eficaz!

Para mais detalhes sobre o pagamento e acesso, acesse [aqui](https://devopeiros.com.br/duvidas-frequentes/).