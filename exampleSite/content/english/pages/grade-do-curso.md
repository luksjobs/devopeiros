---
title: "Grade do Curso"
# meta title
meta_title: ""
# meta description
description: "Nesta página, você encontrará mais informações sobre a Grade do Curso"
# save as draft
draft: false
---

### Grade do Curso: Aprenda tudo sobre Docker do básico ao avançado e dê start na sua jornada Devops!

Este curso abrangente oferece a oportunidade de dominar o **Docker** 🐋, uma das tecnologias mais essenciais no mundo **DevOps**. Com uma abordagem prática e progressiva, você será capacitado a *compreender, implantar e gerenciar containers Docker,* independentemente do seu nível de experiência prévia. O conteúdo abrange desde conceitos fundamentais até tópicos avançados, capacitando você a transformar sua infraestrutura de desenvolvimento e implantação de aplicativos.

{{< accordion "🎥 Aula 01 - Introdução" >}}

Nesta jornada emocionante, exploraremos o mundo do Desenvolvimento e Operações de software, mais conhecido como **DevOps**. Esta primeira aula serve como um ponto de partida, onde mergulharemos nas bases do DevOps e entenderemos por que essa abordagem se tornou fundamental no cenário de desenvolvimento de software moderno. 

{{< /accordion >}}

{{< accordion "🎥 Aula 02 - Introdução ao Linux" >}}

Explore as bases do sistema operacional Linux nesta aula introdutória. Aprenda sobre a história do Linux, suas diferentes distribuições, comandos básicos de terminal e a estrutura de diretórios. Esta aula é ideal para iniciantes que desejam adquirir conhecimentos essenciais sobre o Linux, um sistema operacional de código aberto amplamente utilizado. 

{{< /accordion >}}

{{< accordion "🎥 Aula 03 - Aprendendo a trabalhar com Git" >}}

Nesta aula, vamos mergulhar no fascinante mundo do **Git**, uma das ferramentas mais essenciais para qualquer desenvolvedor de software. Você aprenderá como usar o Git para rastrear e gerenciar o código-fonte do seu projeto, colaborar eficientemente com outros membros da equipe e controlar as versões do seu software.  

{{< /accordion >}}

{{< accordion "🎥 Aula 4 - Criação de máquinas virtuais utilizando o Vagrant" >}}

Aprenda a criar e gerenciar máquinas virtuais de forma eficiente e consistente com o **Vagrant**. Nesta aula, você descobrirá como esta poderosa ferramenta automatiza a configuração de ambientes de desenvolvimento, permitindo que você crie máquinas virtuais sob demanda. 

{{< /accordion >}}

{{< accordion "🎥 Aula 5 - Instalação e configuração do Docker no Windows e Linux" >}}

Nesta etapa crucial do curso, você aprenderá como instalar e configurar o **Docker** em sistemas Windows e Linux. O Docker é uma ferramenta fundamental no mundo DevOps e na virtualização de contêineres, e é essencial para criar, implantar e gerenciar aplicativos de forma eficiente e isolada.

- Criação de **Dockerfiles** (imagens) e **Docker Compose**.
- Entendendo o Network do Docker e o uso de Volumes.
- Registro de Imagens Docker (**Harbor** e **Docker Hub**).
- Orquestração e gerenciamento de clusters utilizando o **Docker Swarm**.

{{< /accordion >}}


{{< accordion "🎥 Aula 6 - Entrega Contínua (CI/CD): Criação de Pipelines" >}}

Nesta empolgante sexta aula, exploraremos a essência da entrega contínua por meio da criação de **pipelines** automatizadas. Vamos mergulhar no mundo da *integração contínua e entrega contínua* (CI/CD), explorando como o **GitLab CI/CD** e o **GitHub Actions** podem ser usados para criar e orquestrar pipelines eficazes!
{{< /accordion >}}

{{< accordion "🎥 Aula 7 - Infraestrutura como Código: Ansible" >}}

Na sétima aula, exploraremos o conceito de '*Infraestrutura como Código*' e aprenderemos a automatizar a configuração dos nossos ambientes. Utilizando a poderosa ferramenta **Ansible**, demonstraremos como automatizar a instalação do **Docker** e **Vagrant** em hosts. Você descobrirá como transformar tarefas de gerenciamento de infraestrutura em código, permitindo uma administração mais eficiente e escalável. Prepare-se para simplificar a configuração dos seus ambientes de desenvolvimento e produção enquanto ganha habilidades valiosas em automação e orquestração.
{{< /accordion >}}

{{< accordion "🎥 Aula 8 - Criação de Dashboards com o Grafana e configuração de dashboards com o Elastic Search" >}}

Na oitava aula, mergulharemos no mundo da visualização de dados e monitoramento com as poderosas ferramentas **Grafana** e **Elastic Search**. Aprenderemos a criar painéis dinâmicos e informativos com o Grafana, permitindo que você acompanhe métricas vitais de forma intuitiva. Além disso, exploraremos a configuração de dashboards com o Elastic Search, fornecendo insights valiosos a partir dos seus dados. Esta aula oferecerá habilidades essenciais para o monitoramento eficaz de sistemas e a tomada de decisões informadas.
{{< /accordion >}}

{{< accordion "🎥 Aula 9 - Criação de Ambiente de Containers na AWS: hospendando nossa aplicação na nuvem" >}}

Na nona aula, daremos um grande passo em direção à nuvem ao explorar como *criar um ambiente de containers na AWS (Amazon Web Services)*. Aprenda a hospedar e gerenciar sua aplicação na nuvem de forma eficiente e escalável, aproveitando as capacidades de contêineres. Esta aula proporcionará uma visão prática de como aproveitar a infraestrutura em nuvem para garantir alta disponibilidade e escalabilidade para sua aplicação, preparando-o para o mundo da computação em nuvem.
{{< /accordion >}}

{{< accordion "🎥 Aula 10 - Simulado final e Recaptução de tudo o que vimos no curso" >}}

Na décima e última aula, é hora de testar seu conhecimento com um simulado final abrangente que aborda os principais conceitos e ferramentas de DevOps que exploramos ao longo do curso. Após o simulado, faremos uma recapitulação completa, revisitando os tópicos essenciais e as habilidades que você adquiriu durante nossa jornada. Esta aula proporcionará a oportunidade de consolidar seu entendimento sobre DevOps e garantir que você esteja preparado para aplicar essas valiosas práticas e ferramentas em seu trabalho futuro.
{{< /accordion >}}

### Benefícios Adicionais:

- Acesso a **laboratórios práticos** e **exercícios** para reforçar o aprendizado.
- Instrutores experientes com amplo conhecimento em **Docker** e **DevOps**.
- Certificado de conclusão para validar suas habilidades.
- Prepare-se para impulsionar sua carreira no mundo DevOps e aprimorar suas habilidades em containers Docker com este curso abrangente!