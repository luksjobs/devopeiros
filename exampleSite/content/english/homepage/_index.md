---
# Banner
banner:
  title: "Aprenda tudo sobre Docker do básico ao avançado e dê start na sua jornada Devops!"
  content: "Durante o curso, você aprenderá desde os conceitos fundamentais do Docker até tópicos mais avançados, como orquestração de contêineres, implantação em nuvem e integração contínua."
  #image: "/images/banner.png"
  button:
    enable: true
    label: "Adquirir Curso"
    link: "https://devopeiros.com.br/pagamento/ "

# Features
features:
  - title: "Público alvo do curso"
    image: "/images/rocket.gif"
    content: "O curso é adequado tanto para iniciantes que desejam aprender sobre os conceitos mais aquecidos da área de Devops quanto para profissionais de TI que desejam aprimorar mais ainda suas habilidades nessa área."
    bulletpoints:
      - "No curso você encontrará o Roadmap completo para quem quer iniciar ou quer avançar em suas habilidades no mundo do **Devops**"
      - "As aulas serão ao vivo (online) na segunda, quarta e sexta das 20:30h às 22h de forma interativa e com disponibilidade da gravação do vídeo ao final de cada aula"
      - "Ambiente de comunicação direta com o professor, onde você pode tirar suas dúvidas diretamente!"
      - "Será feito envio contínuo de conteúdos e materiais através do seu e-mail cadastrado no Hotmart."
    button:
      enable: true
      label: "Quero mais informações"
      link: "https://api.whatsapp.com/send?phone=5584992248571&text=Ol%C3%A1,%20tenho%20interesse%20em%20saber%20mais%20sobre%20o%20curso%20oferecido%20no%20site%20do%20Devopeiros!%20:)"

  - title: "Conteúdo do Curso"
    image: "/images/mortarboard.gif"
    content: "Este curso abrangente de Devops foi projetado para fornecer uma compreensão completa das tecnologias de contêineres, versionamento de código, pipelines e infraestrutura como código, desde os conceitos básicos até os tópicos mais avançados. Durante o curso, você irá explorar os seguintes tópicos:"
    bulletpoints:
      - "Conceitos sobre o Universo do Devops no âmbito de trabalho."
      - "Introdução ao Linux - Instalação do WSL e Comandos Básicos"
      - "Introdução ao Git: O que é o Git e por que é importante?"
      - "Utilizando repositórios do **Gitlab** ou **Github**."
      - "Criação de máquinas virtuais utilizando o **Vagrant**."
      - "Instalação e configuração do **Docker** no Windows e Linux."
      - "Criação de **Dockerfiles** (imagens) e **Docker Compose.**"
      - "Entendendo o **Network** do Docker e o uso de **Volumes**."
      - "Registro de **Imagens** Docker."
      - "Orquestração e gerenciamento de clusters utilizando o **Docker Swarm**."
      - "Exploração de práticas recomendadas para implantação e gerenciamento de contêineres em ambientes de produção."
      - "**Entrega Contínua** (CI/CD): A entrega contínua é a prática de liberar software de forma rápida, frequente e confiável."
      - "**Infraestrutura como Código**: utilizaremos o **Ansible** para automatizar a instalação do Docker e Vagrant nos hosts."
      - "Criação de Dashboards com o **Grafana** e configuração de dashboards com o **Elastic Search**"
      - "Criação de Ambiente de Containers na AWS: hospendando nossa aplicação na nuvem"
    button:
      enable: true
      label: "Veja mais detalhes na brochura do curso"
      link: "https://api.whatsapp.com/send?phone=5584992248571&text=Ol%C3%A1,%20tenho%20interesse%20em%20saber%20mais%20sobre%20o%20curso%20oferecido%20no%20site%20do%20Devopeiros!%20:)"

  - title: "Razões para investir nesse curso"
    image: "/images/save-money.gif"
    content: "Esse é um curso 'Hands-on', mão na massa sem enrolações, montagem de laboratórios e testes feitos na raça! Ao concluir este curso, você estará equipado com as habilidades necessárias para criar, implantar e gerenciar aplicativos em contêineres Docker, desde o nível básico até os tópicos avançados de orquestração e implantação."
    bulletpoints:
      - "O curso oferece uma ampla gama de conhecimentos e habilidades essenciais para implementar com sucesso práticas DevOps."
      - "Ao explorar práticas recomendadas para implantação e gerenciamento de contêineres em ambientes de produção, você estará mais bem preparado para lidar com cenários reais e garantir a estabilidade e a disponibilidade dos sistemas."
      - "O curso abrange o uso de ferramentas amplamente adotadas no mercado, como Git, GitLab, GitHub, Vagrant, Docker, Docker Compose, Docker Swarm, Ansible, Grafana e Elastic Search. Essas ferramentas são essenciais para a implementação de práticas DevOps eficientes."
    button:
      enable: false
      label: ""
      link: ""
---