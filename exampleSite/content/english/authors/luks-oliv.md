---
title: Lucas Oliveira Dantas
email: luksjobs@email.com
image: "/images/avatar.png"
description: curta descrição sobre o instrutor
social:
  - name: gitlab
    icon: fa-brands fa-gitlab
    link: https://gitlab.com/luksjobs

  - name: twitter
    icon: fa-brands fa-twitter
    link: https://twitter.com/luksjobs

  - name: linkedin
    icon: fa-brands fa-linkedin
    link: https://www.linkedin.com/in/luksjobs/

  - name: blog
    icon: fa-brans fa-house
    link: https://luks.dev.br
---

"Profissional de DevOps com mais de 10 anos de experiência. Especializado em implementar pipelines **CI/CD** eficientes, **gerenciar infraestrutura locais e em nuvem** e otimizar o desempenho do sistema. Habilidades sólidas de resolução de problemas e uma paixão por impulsionar melhorias contínuas. Histórico comprovado de entrega bem-sucedida de soluções escaláveis e confiáveis."